// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Btc is ERC20 {
    constructor() ERC20("Bitcoin", "BTC") {}

    function drip() external {
        _mint(msg.sender, 10 ether);
    }
}
