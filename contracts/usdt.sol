// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Usdt is ERC20 {
    constructor() ERC20("Usdt", "USDT") {}

    function drip() external {
        _mint(msg.sender, 10000 ether);
    }
}
