# Spectrr Finance Testnet Faucet Contracts

Contracts for Spectrr Finance Testnet. It can drip the following tokens:
- Bitcoin (BTC)
- Fantom (FTM)
- Matic (MATIC)
- Ethereum (ETH)
- Usdt (USDT)

# Installation

TODO

# Development

TODO

# Disclaimer

- TOKENS HAVE ABSOLUTELY NO REAL VALUE,
THEY ARE ONLY USED FOR TESTING PURPOSE.

# Contributing

Feel free to contribute in any way to this project.
Pull requests, comments, and emails are all appreciated.

# Contact

email: supergrayfly@proton.me

# License

BSD-3-Clause
