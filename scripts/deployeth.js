const hre = require('hardhat');

async function main() {
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);

  const Eth = await hre.ethers.getContractFactory('Eth');
  const eth = await Eth.deploy();

  await eth.deployed();

  console.log(
    'Deployed Contract (ETH) to:',
    eth.address + ',',
    'Timestamp: ' + currentTimestampInSeconds
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
