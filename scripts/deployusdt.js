const hre = require('hardhat');

async function main() {
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);

  const Usdt = await hre.ethers.getContractFactory('Usdt');
  const usdt = await Usdt.deploy();

  await usdt.deployed();

  console.log(
    'Deployed Contract (USDT) to:',
    usdt.address + ',',
    'Timestamp: ' + currentTimestampInSeconds
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
