const hre = require('hardhat');

async function main() {
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);

  const Btc = await hre.ethers.getContractFactory('Btc');
  const btc = await Btc.deploy();

  await btc.deployed();

  console.log(
    'Deployed Contract (BTC) to:',
    btc.address + ',',
    'Timestamp: ' + currentTimestampInSeconds
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
