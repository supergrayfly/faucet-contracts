const hre = require('hardhat');

async function main() {
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);

  const Matic = await hre.ethers.getContractFactory('Matic');
  const matic = await Matic.deploy();

  await matic.deployed();

  console.log(
    'Deployed Contract (MATIC) to:',
    matic.address + ',',
    'Timestamp: ' + currentTimestampInSeconds
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
