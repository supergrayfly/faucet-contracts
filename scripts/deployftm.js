const hre = require('hardhat');

async function main() {
  const currentTimestampInSeconds = Math.round(Date.now() / 1000);

  const Ftm = await hre.ethers.getContractFactory('Ftm');
  const ftm = await Ftm.deploy();

  await ftm.deployed();

  console.log(
    'Deployed Contract (FTM) to:',
    ftm.address + ',',
    'Timestamp: ' + currentTimestampInSeconds
  );
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
